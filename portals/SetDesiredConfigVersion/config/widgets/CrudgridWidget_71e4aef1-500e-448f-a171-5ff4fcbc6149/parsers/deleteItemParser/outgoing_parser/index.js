/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  // using a Collection datasource

    return datasources.MY_COLLECTION_DS.sendData(ctx.widget)
  

  // using a Code Service datasource

      return datasources.MY_DELETE_CODE_SERVICE_DS.sendData({ cbQuery: ctx.widget.query.query }).then((res) => {
          if(!res.success) {
            console.error(res.results)
            CB_PORTAL.createNotification({
              className: "error",
              title: "Delete failed",
              message: res.results,
              dismissAfter: 5000
            })
          }
      })
    
}