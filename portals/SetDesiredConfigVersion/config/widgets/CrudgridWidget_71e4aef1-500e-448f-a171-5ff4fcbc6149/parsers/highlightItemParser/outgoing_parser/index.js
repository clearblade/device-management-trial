/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  console.log(ctx.widget.highlightId)
    // to use the highlighted item out side of this widget, track it's value in a local variable
    // to control the highlighted item from outside, you can also read this value in "Grid Data" { data: [], count: 0, highlightId: datasources.MY_LOCAL_VARIABLE_DS.latestData()}
    // datasources.MY_LOCAL_VARIABLE_DS.sendData(ctx.widget.highlightId)
    
}