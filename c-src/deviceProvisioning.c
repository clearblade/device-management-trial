#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "clearblade.h"
#include "MQTTAsync.h"
#include "json_parser.h"

char *systemKey = "ae9fe6fc0bf0ebe2ceeec5c0d4bf01";
char *systemSecret = "AE9FE6FC0BDCB7F19FFCD68EF5BE01";
char *platformUrl = "https://community.clearblade.com";
char *messagingUrl = "tcp://community.clearblade.com:1883";
char *activeKey = "abc123";

char *provisioningTopic = "provisioning";
char *tokenFilenamePrefix = "token-";

char tokenFileName[100];

int qualityOfService = 0;

void main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Device name not provided in cmd line args.\n");
    }

    char *deviceName = argv[1];

    void mqttOnConnect(void *context, MQTTAsync_successData *response)
    {
        printf("Connected to MQTT Broker!\n");
        extern int finished;
        finished = 1;
    };

    int messageArrivedCallback(void *context, char *topicName, int topicLen, MQTTAsync_message *message)
    {
        char *msgString = (*message).payload;

        if (*topicName == *provisioningTopic)
        {
            printf("Provisioned! received back: %s\n", msgString);

            // If deviceName of this device was returned, then this device has been provisioned
            if (*msgString == *deviceName)
            {
                unsubscribeFromTopic(provisioningTopic);
                printf("Unsubscribed from 'provisioning'!\n");
            }
        }
        return 1;
    };

    void initCallback(bool error, char *result)
    {
        if (error)
        {
            printf("ClearBlade init failed %s\n", result);
            exit(-1);
        }
        else
        {
            printf("ClearBlade Init Succeeded\nAuth token: %s\n", result);

            strcpy(tokenFileName, tokenFilenamePrefix);
            strcat(tokenFileName, deviceName);

            FILE *fPtr;
            fPtr = fopen(tokenFileName, "w");

            if (fPtr == NULL)
            {
                /* File not created hence exit */
                printf("Unable to create file.\n");
                exit(EXIT_FAILURE);
            }

            /* Write data to file */
            fputs(result, fPtr);

            /* Close file to save file data */
            fclose(fPtr);

            /* Success message */
            printf("Token saved successfully.\n");
        };
    };

    initializeClearBladeAsDevice(systemKey, systemSecret, platformUrl, messagingUrl, deviceName, activeKey, initCallback);

    connectToMQTT(deviceName, qualityOfService, mqttOnConnect, messageArrivedCallback);

    subscribeToTopic(provisioningTopic, 0);

    getchar();
}