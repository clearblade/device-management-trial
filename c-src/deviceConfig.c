#include <stdio.h>
#include <string.h>
#include <math.h>
#include "clearblade.h"
#include "MQTTAsync.h"
#include "json_parser.h"

char *systemKey = "ae9fe6fc0bf0ebe2ceeec5c0d4bf01";
char *systemSecret = "AE9FE6FC0BDCB7F19FFCD68EF5BE01";
char *platformUrl = "https://community.clearblade.com";
char *messagingUrl = "tcp://community.clearblade.com:1883";
char *activeKey = "abc123";

char *statusTopicPrefix = "device/status/";
char *configTopicPrefix = "device/config/";
char *dataTopicPrefix = "device/data/";

char statusTopic[100];
char configTopic[100];
char dataTopic[100];

char chosenUnit[100];
char tempStr[12];

void main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Device name not provided in cmd line args.\n");
    };

    char *deviceName = argv[1];

    strcpy(statusTopic, statusTopicPrefix);
    strcat(statusTopic, deviceName);

    strcpy(configTopic, configTopicPrefix);
    strcat(configTopic, deviceName);

    strcpy(dataTopic, dataTopicPrefix);
    strcat(dataTopic, deviceName);

    void mqttOnConnect(void *context, MQTTAsync_successData *response)
    {
        printf("Connected to MQTT Broker!\n");
        extern int finished;
        finished = 1;
    };

    int messageArrivedCallback(void *context, char *topicName, int topicLen, MQTTAsync_message *message)
    {
        char *msgString = (*message).payload;

        printf("%s\n", msgString);

        if (*topicName == *configTopic)
        {
            char *unit = getPropertyValueFromJson(msgString, "unit");

            strcpy(chosenUnit, unit);

            printf("Sending acknowledgement that config has changed...\n\n");

            publishMessage("configchanged", statusTopic, 0, false);

            // If deviceName of this device was returned, then this device has been provisioned

            // unsubscribeFromTopic(configTopic);
        }
        return 1;
    };

    void initCallback(bool error, char *result)
    {
        if (error)
        {
            printf("ClearBlade init failed %s\n", result);
            exit(-1);
        }
        else
        {
            printf(result);
        }
    };

    initializeClearBladeAsDevice(systemKey, systemSecret, platformUrl, messagingUrl, deviceName, activeKey, initCallback);

    connectToMQTT(deviceName, 0, mqttOnConnect, messageArrivedCallback);

    subscribeToTopic(configTopic, 0);

    printf("Sending request for desired config...\n\n");
    publishMessage("ready", statusTopic, 0, false);

    int loTemp = 0;
    int hiTemp = 100;
    double tempC;
    double temp;

    for (int i = 0; i < 100; i++)
    {
        tempC = (rand() % (hiTemp - loTemp + 1)) + loTemp;
        if (strcmp(chosenUnit, "farenheit") == 0)
        {
            temp = (tempC * 9 / 5) + 32;
        }
        else if (strcmp(chosenUnit, "kelvin") == 0)
        {
            temp = (tempC + 273.15);
        }
        else
        {
            temp = tempC;
        };
        sprintf(tempStr, "%.2f", temp);
        printf("%s\n", tempStr);
        publishMessage(tempStr, dataTopic, 0, false);
        sleep(2);
    }
}