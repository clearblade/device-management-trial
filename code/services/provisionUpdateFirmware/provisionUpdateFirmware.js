function provisionUpdateFirmware(req,resp){
    ClearBlade.init({request: req});
    const client = new MQTT.Client();
    const subTopic = "$trigger/messaging/device/connected";
    //const provisioningTopicPrefix = "provisioning";
    const provisioningTopic = "provisioning";
    const paramsTopicPrefix = "params";

    const roles = ClearBlade.Roles();
    const permission = ClearBlade.Permissions();

    var err_msg;
    
    client_subscribe(subTopic);

    function processMessage(message, topic) {
        // Get Device Name from topic
        var deviceName = JSON.parse(message).deviceName;
        //var provisioningTopic = provisioningTopicPrefix + "/" + deviceName;
        var paramsTopic = paramsTopicPrefix + "/" + deviceName;
        
        var rolesCb = function (err, data) {
			if (err) {
				log("Parse error : " + JSON.stringify(data));
			} else {
				if (data.length === 0) {
                    // Role does not exist; i.e. first time this device came online
                    if (err) {
                        log("Error adding new role to device; data: " + JSON.stringify(data) + ";err: " + JSON.stringify(err));
                    } else {
                        var roleMeta = {
                            "name": deviceName,
                            "description": deviceName,
                            }

                    var rolesCreateCb = function (err, data) {
                        if (err) {
                            log("Error creating role; data: " + data + "; err: " + JSON.stringify(err));
                        } else {

                            // Set permission for paramsTopic such that CRUD allowed
                            var paramsTopicPermissionsMeta = {
                                "requestType": 7,
                                "resourceType": "topic",
                                "resourceName": paramsTopic
                                }

                            var addParamsTopicPermissionCb = function (err, data) {
                                if (err) {
                                    log("Error adding permission; data: " + data + "; err: " + JSON.stringify(err));
                                } else {    
                                    var addRoleToDeviceCb = function (err, data) {
                                        if (err) {
                                            log("Error adding role to device; data: " + JSON.stringify(data) + ";err: " + JSON.stringify(err));
                                        } else {
                                            // Return deviceName as indication to THAT device that it has been provisioned;
                                            // Note that multiple devices may be subscribed to 'provisioning' this indication
                                            // is only for one of the several
                                            client_publish(provisioningTopic, deviceName);

                                            
                                        }
                                    }
                                
                                    roles.addRoleToDevice(deviceName, deviceName, addRoleToDeviceCb);
                                }
                            }

                            permission.addPermissionToRole(data, paramsTopicPermissionsMeta, addParamsTopicPermissionCb);

                            }
                        };

                        roles.create(roleMeta, rolesCreateCb);
                    }
                
                }
			};
		};
        roles.getByName(deviceName, rolesCb);
    };
    
    function client_publish(pub_topic, message) {
    client.publish(pub_topic, message, 0, true)
        .catch(function (reason) {
            err_msg = "Failed to publish to topic '" + pub_topic + "'. Reason: " + reason.message;
            log(err_msg);
            resp.error(err_msg);
        });
    };

    function client_subscribe(subTopic) {
        function onMessage(subTopic, message) {
            log("Received message on topic '" + subTopic + "': " + message.payload);
            processMessage(message.payload, subTopic);
        };
            
        client.subscribe(subTopic, onMessage).catch(function (reason) {
            err_msg = "Failed to subscribe to topic '" + subTopic + "'. Reason: " + reason.message;
            resp.error(err_msg);
        });
    };
}