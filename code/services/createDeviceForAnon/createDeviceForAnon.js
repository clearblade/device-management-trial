function createDeviceForAnon(req,resp){
    ClearBlade.init({request: req});
    
    const client = new MQTT.Client();
    const subTopic = "createdevice";
    const deviceCredentialsTopic = "devicecredentials";
    
    var err_msg;
    
    client_subscribe(subTopic);

    function processMessage(message, topic) {
        // Get Device Name from message
        var deviceName = message;
        var device = {
            name: deviceName,
            active_key: deviceName.split('').reverse().join(''),
            type: "",
            state: "",
            enabled: true,
            allow_key_auth: true,
            allow_certificate_auth: true,
            desired_config_version: 3
        };
        var createDeviceCb = function(err, data) {
		    if(err){
			    log("Unable to create device; data: " + JSON.stringify(data) + "; err: " + JSON.stringify(err));
		    }
		    client_publish(deviceCredentialsTopic, JSON.stringify({ deviceName, activeKey: device.active_key }));
	    };

	    ClearBlade.createDevice(device.name, device, false, createDeviceCb);
    };
    
    function client_publish(pub_topic, message) {
    client.publish(pub_topic, message, 0, false)
        .catch(function (reason) {
            err_msg = "Failed to publish to topic '" + pub_topic + "'. Reason: " + reason.message;
            log(err_msg);
            resp.error(err_msg);
        });
    };

    function client_subscribe(subTopic) {
        function onMessage(subTopic, message) {
            log("Received message on topic '" + subTopic + "': " + message.payload);
            processMessage(message.payload, subTopic);
        };
            
        client.subscribe(subTopic, onMessage).catch(function (reason) {
            err_msg = "Failed to subscribe to topic '" + subTopic + "'. Reason: " + reason.message;
            resp.error(err_msg);
        });
    };
}