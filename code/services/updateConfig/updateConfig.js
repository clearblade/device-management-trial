function updateConfig(req,resp){
    ClearBlade.init({request: req});
    const configColl = ClearBlade.Collection({collectionName: "Config"});

    const client = new MQTT.Client();
    const subTopic = "device/status/+";
    
    var err_msg;
    
    client_subscribe(subTopic);

    function processMessage(message, topic) {
        var deviceName = topic.split('/')[2];
        // Get Device Name from topic
        log(deviceName);
        var getDeviceCb = function (err, data) {
            if (err) {
            log("Error getting device info; data: " + JSON.stringify(data) + "; err: " + JSON.stringify(err));
            } else {
                // Extract desiredCfgVersion from devices table
                var desiredCfgVersion = data.desired_config_version;
                log("Desired Config: " + desiredCfgVersion);
                // Get config
                var configCollCb = function (err, data) {
                    if (err) {
                        log("Config Collection Fetch error; data: " + JSON.stringify(data) + "; err: " + JSON.stringify(err));
                    } else {
                        // If message from device was "sendconfig", then send config.
                        // If message from device was "configchanged" then change current_config_version in Devices
                        switch(message) {
                            case "ready":
                                var paramsToSend = data.DATA[0].params;
                                client_publish("device/config/" + deviceName, paramsToSend);
                                log("published");
                                break;
                            case "configchanged":
                                var updateDeviceCb = function(err, data) {
                                    if(err) {
                                        log("Unable to update device; data: " + JSON.stringify(data) + "; err: " + JSON.stringify(err));
                                    }
	                            }
                                ClearBlade.updateDevice(deviceName, {"current_config_version": desiredCfgVersion}, false, updateDeviceCb);
                                break;
                        }
                    }
                };

                var query = ClearBlade.Query();
                query.equalTo("version", desiredCfgVersion);
                configColl.fetch(query, configCollCb);
            }
        };
        ClearBlade.getDeviceByName(deviceName, getDeviceCb);
    };
    
    function client_publish(pub_topic, message) {
    client.publish(pub_topic, message, 0, false)
        .catch(function (reason) {
            err_msg = "Failed to publish to topic '" + pub_topic + "'. Reason: " + reason.message;
            log(err_msg);
            resp.error(err_msg);
        });
    };

    function client_subscribe(subTopic) {
        function onMessage(subTopic, message) {
            log("Received message on topic '" + subTopic + "': " + message.payload);
            processMessage(message.payload, subTopic);
        };
            
        client.subscribe(subTopic, onMessage).catch(function (reason) {
            err_msg = "Failed to subscribe to topic '" + subTopic + "'. Reason: " + reason.message;
            resp.error(err_msg);
        });
    };
}